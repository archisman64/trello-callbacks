/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const fs = require('fs');
const path = require('path');

// const boardsPath = path.resolve('./data/boards.json');
// const cardsPath = path.resolve('./cards.json');
const listsPath = path.resolve('./data/lists.json');

// generating random seconds between max and min
const max = 5;
const min = 2;
const randomSeconds = (Math.floor(Math.random() * (max - min + 1)) + min) * 1000;

function getAllLists (boardID, callback) {
    setTimeout(() => {
        
        fs.readFile(listsPath, 'utf-8', (error, data) => {
            if(error) {
                callback(error);
            } else {
                const listData = JSON.parse(data);
                // console.log(listData);
                const requiredList = Object.entries(listData).filter((data) => {
                    const id = data[0];
                    const list = data[1];
                    return id === boardID;
                })
                
                callback(null, requiredList);
            }
        })
    }, randomSeconds);
};

module.exports = getAllLists;