/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const fs = require('fs');
const path = require('path');

const boardsPath = path.resolve('./data/boards.json');
// const cardsPath = path.resolve('./data/cards.json');
// const listsPath = path.resolve('./data/lists.json');

// requiring previous functions
const getAllLists = require('./callback2.cjs');
const getAllCards = require('./callback3.cjs');

function getThanosData (label, callback) {
    if(!label){
        label = 'thanos';
    }

    fs.readFile(boardsPath, 'utf-8', (err, data) => {
        if(err) {
            callback(err);
        } else {
            const boardData = JSON.parse(data);
            const thanos = boardData.find((item) => {
                if(item.name.toLowerCase() === label) {
                    return true;
                } else {
                    return false;
                }
            })
            const thanos_id = thanos.id;
            
            // fetching all the lists of thanos
            getAllLists(thanos_id, (err, data) => {
                if(err) {
                    console.log(err);
                } else {
                    console.log('thanos lists:', data[0]);
                    const listArray = data[0][1];

                    // fetching the cards for all the lists
                    listArray.forEach((list) => {
                        getAllCards(list.id, (err, data) => {
                            if(err) {
                                console.log(err);
                            } else {
                                console.log(`cards in ${list.name} list:`, data[0]);
                            }
                        });
                    })
                }
            })
        }
    })
}

module.exports = getThanosData;