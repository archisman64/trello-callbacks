/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

const fs = require('fs');
const path = require('path');

// const boardsPath = path.resolve('./data/boards.json');
const cardsPath = path.resolve('./data/cards.json');
// const listsPath = path.resolve('./data/lists.json');

// generating random seconds between max and min
const max = 5;
const min = 2;
const randomSeconds = (Math.floor(Math.random() * (max - min + 1)) + min) * 1000;

function getAllCards (listID, callback) {
    setTimeout(() => {
        
        fs.readFile(cardsPath, 'utf-8', (error, data) => {
            if(error) {
                callback(error);
            } else {
                const cardsData = JSON.parse(data);
                // console.log(cardsData);
                const requiredList = Object.entries(cardsData).filter((data) => {
                    const id = data[0];
                    const cardsList = data[1];
                    return id === listID;
                })
                callback(null, requiredList);
            }
        })
    }, randomSeconds);
};

module.exports = getAllCards;