/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

const fs = require('fs');
const path = require('path');

const boardsPath = path.resolve('./data/boards.json');
// const cardsPath = path.resolve('./cards.json');
// const listsPath = path.resolve('./lists.json');

// generating random seconds between max and min
const max = 5;
const min = 2;
const randomSeconds = (Math.floor(Math.random() * (max - min + 1)) + min) * 1000;
// console.log(randomSeconds);

function getBoardInfo (boardID, callback) {
    setTimeout(() => {
        
        fs.readFile(boardsPath, 'utf-8', (error, data) => {
            if(error) {
                callback(error);
            } else {
                const boardData = JSON.parse(data);
                const requiredBoard = boardData.filter((element) => {
                    // console.log('element:', element);
                    if(element.id === boardID) {
                        return true;
                    } else {
                        return false;
                    }
                })
                callback(null, requiredBoard);
            }
        })
    }, randomSeconds);
};

module.exports = getBoardInfo;