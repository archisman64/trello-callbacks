/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/
const fs = require('fs');
const path = require('path');

const boardsPath = path.resolve('./data/boards.json');
// const cardsPath = path.resolve('./data/cards.json');
// const listsPath = path.resolve('./data/lists.json');

// requiring previous functions
const getAllLists = require('./callback2.cjs');
const getAllCards = require('./callback3.cjs');

function getThanosData (label, callback) {
    if(!label){
        label = 'thanos';
    }

    fs.readFile(boardsPath, 'utf-8', (err, data) => {
        if(err) {
            callback(err);
        } else {
            const boardData = JSON.parse(data);
            const thanos = boardData.find((item) => {
                if(item.name.toLowerCase() === label) {
                    return true;
                } else {
                    return false;
                }
            })
            const thanos_id = thanos.id;
            
            getAllLists(thanos_id, (err, data) => {
                if(err) {
                    console.log(err);
                } else {
                    console.log('thanos lists:', data[0]);
                    const listArray = data[0][1];
                    // filtering mind list
                    const mindList = listArray.find((list) => {
                        if(list.name.toLowerCase() === 'mind'){
                            return true;
                        } else {
                            return false;
                        }
                    });

                    // fetching the cards for mind list
                    getAllCards(mindList.id, (err, data) => {
                        if(err) {
                            console.log(err);
                        } else {
                            console.log('cards in mind list:', data[0]);
                        }
                    });
                }
            })
        }
    })
}

module.exports = getThanosData;