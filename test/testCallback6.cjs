const problem6 = require('../callback6.cjs');

const label = 'thanos';

function callback (err, data) {
    if(err) {
        console.log(err);
    } else {
        console.log(data);
    }
}

problem6(label, callback);