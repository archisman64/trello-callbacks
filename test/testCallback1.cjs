const problem1 = require('../callback1.cjs');

function callback (error, data) {
    if(error) {
        console.log(error);
    } else {
        console.log('required board data:', data[0]);
    }
}

problem1("abc122dc", callback);