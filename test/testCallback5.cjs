const problem5 = require('../callback5.cjs');

const label = 'thanos';

function callback (err, data) {
    if(err) {
        console.log(err);
    } else {
        console.log(data);
    }
}

problem5(label, callback);