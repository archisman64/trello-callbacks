const problem4 = require('../callback4.cjs');

const label = 'thanos';

function callback (err, data) {
    if(err) {
        console.log(err);
    } else {
        console.log(data);
    }
}

problem4(label, callback);