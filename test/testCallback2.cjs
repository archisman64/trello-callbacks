const problem2 = require('../callback2.cjs');

function callback (error, data) {
    if(error) {
        console.log(error);
    } else {
        console.log('required list data:', data[0]);
    }
}

problem2("mcu453ed", callback);