const problem3 = require('../callback3.cjs');

const listID = "qwsa221";

function callback (error, data) {
    if(error) {
        console.log(error);
    } else {
        console.log(`cards belonging to listID ${listID}:`, data[0]);
    }
}

problem3(listID, callback);